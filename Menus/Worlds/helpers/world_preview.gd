class_name WorldPreview
var ClickyPanel = preload("res://addons/CustomControls/controls/ClickyPanel.gd")
var referenced_world : GameMetadata
var preview_panel : PanelContainer
signal preview_selected(id)

func _init(world : GameMetadata):
	referenced_world = world


func _generate():
	#panel
	preview_panel = ClickyPanel.new()
	#preview_panel.input_pass_on_modal_close_click = false
# warning-ignore:return_value_discarded
	preview_panel.connect("clicky", Callable(self, "_clicky"))
	#stack
	var layout = VBoxContainer.new()
	#layout.input_pass_on_modal_close_click = true
	preview_panel.add_child(layout)
	#picture
	var icon = TextureRect.new()
	icon.texture = referenced_world.previewImage
	#icon.input_pass_on_modal_close_click = true
	layout.add_child(icon)
	#text
	var GameNameLabel = Label.new()
	GameNameLabel.text = referenced_world.name
	layout.add_child(GameNameLabel)
	
func get_preview() -> PanelContainer:
	if !preview_panel:
		_generate()
	return preview_panel


func _clicky():
	emit_signal("preview_selected",referenced_world.id)
