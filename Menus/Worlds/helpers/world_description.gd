extends PanelContainer

class_name WorldDescription

var referenced_world : GameMetadata
var image_box:  TextureRect
var descriptionBox : TextEdit

#buttons
var settingsButton : Button
var modsButton : Button
var playButton : Button

func _init():
	size_flags_horizontal += PanelContainer.SIZE_EXPAND
	size_flags_vertical += PanelContainer.SIZE_EXPAND

	var big_layout = HBoxContainer.new()
	add_child(big_layout)
	
	descriptionBox = TextEdit.new()
	descriptionBox.size_flags_horizontal += descriptionBox.SIZE_EXPAND
	descriptionBox.size_flags_vertical += descriptionBox.SIZE_EXPAND
	big_layout.add_child(descriptionBox)
	
	var side_panel = VBoxContainer.new()
	big_layout.add_child(side_panel)
	
	image_box = TextureRect.new()
	side_panel.add_child(image_box)
	
	playButton = _newButton("Play",side_panel,"_play_button_pressed")
	modsButton = _newButton("Mods",side_panel,"_mods_button_pressed")
	settingsButton = _newButton("Settings",side_panel,"_settings_button_pressed")
	
func _newButton(text :String, parent : Node, callback : String) :
	var button : Button = Button.new()
	button.text = text
	parent.add_child(button)
# warning-ignore:return_value_discarded
	button.connect("pressed", Callable(self, callback))
	return button

func _settings_button_pressed():
	pass

func _mods_button_pressed():
	pass

func _play_button_pressed():
	SignalBus.raiseSignal("LaunchGame",{"GameInfo":referenced_world})
	pass

func getBox():
	return self
	
func _update():
	image_box.texture = referenced_world.previewImage
	descriptionBox.text = referenced_world.description
	pass
	
func update_contents(world : GameMetadata):
	referenced_world = world
	_update()
