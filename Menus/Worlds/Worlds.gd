extends PanelContainer
class_name WorldsMenu
@export var defaultIcon : CompressedTexture2D 
var GameMetadata = preload("res://Classes/GameObjects/game_metadata.gd")
var worlds = {}
var worldPreviews = {}
var description_box : WorldDescription
#warning-ignore:UNUSED_SIGNAL
signal changePage(page)
	
func _load_worlds():
	worlds = FileEngine.get_saves()
	var newGame = GameMetadata.new()
	newGame.name = "Start new game"
	newGame.description = "Enter a new world, fresh from the system!"
	worlds[newGame.id] = newGame
	
# Called when the node enters the scene tree for the first time.
func _ready():
	_load_worlds()
	var preview_holder = get_node("VBoxContainer/SelectionPanel/SelectionScroll/SelectionHolder")
	for world in worlds:
		var preview = WorldPreview.new(worlds[world])
		worldPreviews[world] = preview
		preview_holder.add_child(preview.get_preview())
		preview.connect("preview_selected", Callable(self, "set_preview"))
	description_box = WorldDescription.new()
	var layout = get_node("VBoxContainer")
	layout.add_child(description_box)
	
func set_preview(world):
	description_box.update_contents(worlds[world])

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
