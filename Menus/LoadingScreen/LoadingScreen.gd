extends PanelContainer

var formatBox : VBoxContainer
var titleMenu 
var menuBox : CenterContainer
var footerLabel 
var parent : TabContainer
var tabsIndex : Array
signal changePage(page)

func _modsButtonPressed():
	change_tab("Mods")


func _settingsButtonPressed():
	change_tab("Settings")


func _creditsButtonPressed():
	change_tab("Credits")


func _siteButtonPressed():
# warning-ignore:return_value_discarded
	OS.shell_open("https://gitlab.com/ThisLifeDevelopment/thislifegodot/-/wikis/Welcome!")


func _worldsButtonPressed():
	change_tab("Worlds")


func change_tab(tab):
	emit_signal("changePage",tab)


func _ready():
	parent = get_parent()
	tabsIndex = []
	var tabs =  parent.get_children()
	for tab in tabs:
		tabsIndex.push_back(tab.name)
	var buttonContainer : VBoxContainer = get_node("layout/PanelContainer2/CenterContainer/PanelContainer/HBoxContainer/Button_Container")
# warning-ignore:return_value_discarded
	buttonContainer.get_node("Mods_Button").connect("pressed", Callable(self, "_modsButtonPressed"))
# warning-ignore:return_value_discarded
	buttonContainer.get_node("Credits_Button").connect("pressed", Callable(self, "_creditsButtonPressed"))
# warning-ignore:return_value_discarded
	buttonContainer.get_node("Settings_Button").connect("pressed", Callable(self, "_settingsButtonPressed"))
# warning-ignore:return_value_discarded
	buttonContainer.get_node("Site_Button").connect("pressed", Callable(self, "_siteButtonPressed"))
# warning-ignore:return_value_discarded
	buttonContainer.get_node("Worlds_Button").connect("pressed", Callable(self, "_worldsButtonPressed"))
