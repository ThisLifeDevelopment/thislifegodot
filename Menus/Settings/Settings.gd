extends PanelContainer
var settingsMenu: VBoxContainer
var settingsList = []
# warning-ignore:unused_signal
signal changePage(page)
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
func buildTitle(titleName) -> PanelContainer:
	var titleBox = PanelContainer.new()
	var titleCenter = CenterContainer.new()
	titleBox.add_child(titleCenter)
	var title = Label.new()
	title.text = titleName
	titleCenter.add_child(title)
	return titleBox

func buildSectionPanel(sectionName,settings) -> PanelContainer:
	var section = PanelContainer.new()
	var stack = VBoxContainer.new()
	section.add_child(stack)
	
	var titleBox = buildTitle(sectionName)
	stack.add_child(titleBox)
	
	var settingsPane = PanelContainer.new()
	stack.add_child(settingsPane)
	
	var settingSplit = HSplitContainer.new()
	settingsPane.add_child(settingSplit)
	var settingLabelStack = VBoxContainer.new()
	settingSplit.add_child(settingLabelStack)
	var settingsStack = VBoxContainer.new()
	settingSplit.add_child(settingsStack)
	for setting_object in settings:
		var builtSetting = setting_builder.new()
		builtSetting.setting_data(setting_object)
		settingLabelStack.add_child(builtSetting.setting_label)
		settingsStack.add_child(builtSetting.setting_field)
		settingsList.push_back(builtSetting)
	
	return section
	
# Called when the node enters the scene tree for the first time.
func _ready():
	settingsMenu = get_node("VBoxContainer/ScrollContainer/SettingsMenu")
	var fileSystem = FileEngine
	var config = fileSystem.configSystem
	#settingsTree.hide_root = true
	for settingCategory in config.configData:
		var sectionPanel : PanelContainer = buildSectionPanel(settingCategory,config.configData[settingCategory])
		settingsMenu.add_child(sectionPanel)
