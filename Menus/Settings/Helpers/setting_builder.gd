class_name setting_builder

var setting_object : setting
var setting_label : Label
var setting_field 
var height = 0
func _init():
	setting_label = Label.new()
# warning-ignore:return_value_discarded
	setting_label.connect("resized", Callable(self, "_height_change"))
	pass
func setting_data(setting_obj):
	setting_object = setting_obj
	setting_label.text = setting_object.setting_name
	if !setting_field:
		setting_field = TextEdit.new()
		setting_field.custom_minimum_size = Vector2(0,20.0)
		setting_field.size_flags_horizontal += TextEdit.SIZE_EXPAND
		setting_field.size_flags_vertical += TextEdit.SIZE_EXPAND
		setting_field.connect("resized", Callable(self, "_height_change"))
		setting_field.connect("ready", Callable(self, "_height_change"))
	setting_field.text = setting_object.setting_value
func _height_change():
	if !setting_field:
		pass
	else:
		var current_size = max(setting_label.custom_minimum_size[1],setting_field.custom_minimum_size[1])
		#if height != current_size:
		height = current_size
		setting_label.custom_minimum_size = Vector2(setting_label.custom_minimum_size[0],height)
		setting_field.custom_minimum_size = Vector2(setting_field.custom_minimum_size[0],height)
	pass
