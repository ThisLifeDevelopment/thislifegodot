extends PanelContainer

var title
var modScroll
# warning-ignore:unused_signal
signal changePage(page)
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func buildTitle():
	var titlePanel : CenterContainer
	titlePanel = CenterContainer.new()
	titlePanel.anchor_right = ANCHOR_END
	titlePanel.anchor_bottom = ANCHOR_END
	
	var titleText : Label
	titleText = Label.new()
	titleText.align = Label.PRESET_CENTER
	titleText.set_size(Vector2(0.165,1))
	titleText.text = "Mods"
	titlePanel.add_child(titleText)
	titlePanel.set_size(Vector2(0.165,1))
	
	return titlePanel
	
func buildModSection():
	var modSection : ScrollContainer
	modSection = ScrollContainer.new()
	modSection.scroll_vertical_enabled = true
	modSection.scroll_horizontal_enabled = true
	return modSection
# Called when the node enters the scene tree for the first time.

func buildControls():
	var controlContainer : HBoxContainer
	controlContainer = HBoxContainer.new()
	return controlContainer

func _ready():
	pass
func no():
	var layout : VBoxContainer
	layout = VBoxContainer.new()
	add_child(layout)
	
	title = buildTitle()
	layout.add_child(title)
	modScroll = buildModSection()
	layout.add_child(modScroll)
	
	layout.add_child(buildControls())
	pass # Replace with function body.

func buildModBox():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
