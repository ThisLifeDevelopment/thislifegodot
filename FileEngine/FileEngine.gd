extends Node

#const SQLite = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")
var SaveEngine = load("res://FileEngine/SaveEngine.gd")
var saveMachine
var ConfigSystem = load("res://FileEngine/ConfigSystem.gd")
var configSystem
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func get_saves():
	return saveMachine.get_saves()

func saveGame(): #Triggers data move from temporary to perminant storage. Typically done by user.
	saveMachine.save()
func saveObject(type,object):
	saveMachine.storeObject(type, object)
func getObject(type,id):
	saveMachine.getObject(type,id)
func startGame(game_name):
	saveMachine.buildGame(game_name) #Ask for a save "schema" to be generated
	pass
func createSave(save_name:String):
	var folder : setting = configSystem.get_setting("Settings","saveFolder")
	var dir = DirAccess.open("user://")
	dir.make_dir_recursive(folder._get_value())
	#Create folder if not exists:
	saveMachine.initialize(folder._get_value()+save_name)
	#saveMachine.buildGame("test")
	return 
	pass
# Called when the node enters the scene tree for the first time.
func _ready():
	configSystem = ConfigSystem.new()
	add_child(configSystem)
	setSaveMachine(SaveEngine.new())
	createSave("test")
func setSaveMachine(obj):
	if (is_instance_valid(saveMachine)):
		saveMachine.free()
	saveMachine = obj
	add_child(obj)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
