extends Node
## Stores game information in SQL
##
## Provides several functions to allow for objcts to be stored and removed from SQL
class_name SaveEngine

var DB : SQLite = null
var tables = {
	"test": {
		"id": {
			"data_type" : "int",
			"not_null" : true,
			"primary_key": true,
			"auto_increment" : true
		},
		"textValue": {
			"data_type" : "text",
		}
	}
}
var GameID
var built = false
var prefix = {"local":"","auto":"au_"}

#Startup tools
func initialize(fileName):
	DB = SQLite.new()
	DB.path=fileName
	DB.open_db()#Create Database
	DB.query("BEGIN TRANSACTION") #Start Transaction
	generateMetadataTables()
	if !databaseIsCurrent():
		migrateDatabase() #This is unsafe!!! Change this to ask for permission first!!!
		pass
	DB.query("END TRANSACTION") #End Transaction
	pass
	
func get_saves():
	DB.query("SELECT saveName, Created, Updated, ID from `Save_Info`")
	var results = DB.query_result
	var gameMetadata = {}
	for result in results:
		var metadata = GameMetadata.new()
		metadata.name = result.saveName
		#metadata.previewImage = result.previewImage
		#metadata.description = result.description
		metadata.created = result.Created
		metadata.modified = result.Updated
		metadata.id = result.ID
		gameMetadata[result.ID] = metadata
	return gameMetadata
	
func generateMetadataTables():
	var tableInfo = {
		"Metadata": {
			"Property": {
				"data_type" : "text",
				"not_null" : true,
				"primary_key": true,
			},
			"Value": {
				"data_type" : "text",
			}
		}
	}
	#DB.create_table("Save_Info", tableInfo["SaveInfo"])
	#Create Table not used due to lack of support for DATETIME 
	DB.query( "CREATE TABLE IF NOT EXISTS `Save_Info` (" +
		"saveName TEXT UNIQUE," +
		"Created DATETIME," +
		"Updated DATETIME," +
		"previewImage BLOB,"+
		"description TEXT," +
		"ID INTEGER PRIMARY KEY autoincrement" +
		");"
	)
	for table in tableInfo:
		DB.create_table(table,tableInfo[table])
func databaseIsCurrent():
	return true
	pass
func migrateDatabase():
	pass
#Object Access - allow for finding, creating, and saving objects
func storeObject(table,object):
	DB.update_rows(prefix["auto"]+table,"id="+str(object["id"]),object)
	
func deleteObject(table,object): #got it i need a delete column
	var id
	if (typeof(object) == TYPE_DICTIONARY):
		id = object[id]
	else:
		id = object
	DB.update_rows(prefix["auto"]+table,"id = "+str(id), {"del": 1})
func getObject(table,id):
	var rows = []
	for row in tables[table]:
		rows.push_back(row)
	DB.select_rows(prefix["auto"]+table, "id = " + str(id),rows)
	if DB.query_result.size() > 0:
		return DB.query_result[0]
		
	var data = {}
	DB.select_rows(prefix["local"]+table, "id = " + str(id), rows)
	if DB.query_result.size() > 0:
		data = DB.query_result[0]
	else:
		data["id"]=id
	DB.insert_row(prefix["auto"]+table, data)
	return data

func getObjectByProperty(table,lookupInfo):
	var query = "1=1"
	var rows = []
	for term in lookupInfo:
		query = " AND ".join([query,term+" = "+lookupInfo[term]])
	for row in tables[table]:
		rows.push_back(row)
	DB.select_rows(table,query,rows)
	pass
#get a new element that is guaranteed to be an orriginal. Only works with numeric IDs (which will be most). 
func NewObject(table):
	DB.query("select max(id) from "+prefix["auto"]+table)
	var result = DB.query_result[0]
	return getObject(table,result+1)

#Aggregate Actions (Build and launch the game/save everything/)
func buildGame(Game_name : String):
	
	#Getting an if statement in the search isn't going well. I'm just gonna do it the slow way.
	DB.query_with_bindings("SELECT ID from `Save_Info` where saveName = ?;",[Game_name])
	if DB.query_result.size() == 0:
		DB.query_with_bindings(
		"Insert into `Save_Info` (saveName, Created, Updated)" +
		"VALUES(?, DATETIME('NOW'),DATETIME('NOW'));", [Game_name]
		)
		GameID = DB.last_insert_rowid
	else: 
		GameID = DB.query_result[0]["ID"]
	loadGame(GameID)
func save():
	DB.query("BEGIN TRANSACTION")
	var keys
	var results
	var resultList
	#upsert data from autosave tables into long term tables
	for table in tables:
		keys = tables[table].keys()
		keys.push_back("del")
		DB.select_rows(prefix["auto"]+table,"1=1",keys)
		results = DB.query_result
		for result in results: #TODO: See if I can swap this with a multiple upsert to reduce calls
			resultList = []
			for key in keys:
				resultList.pushBack(result[key])
			DB.query("UPSERT into "+ prefix["local"]+table + 
			"("+ ",".join(keys) +")" + " VALUES (" + ",".join(resultList) + ")"
			)
	
	clearAutosaveTables()
	for table in tables:
		DB.query("Delete from " + prefix["local"]+table + "WHERE delete=1")
	makeAutoSaveTables()
	DB.query_with_bindings(
		"Update `Save_Info` "+
		"SET `Updated` = DATETIME('NOW')" +
		"WHERE id=?;", [GameID]
		)
	
	DB.query("END TRANSACTION")
func loadGame(id):
	prefix["local"] = "G" + str(id) + "_"
	makeTables()
	pass
func closeGame():
	clearAutosaveTables()
#Table Prep Tools
func addTable(tableName,tableData):
	tables[tableName] = tableData
	makeTable(prefix["local"],tableName)
	if built:
		makeTable(prefix["auto"],tableName)
		moveLastEntry(tableName)
func makeTables():
	built = true
	for table in tables:
		makeTable(prefix["auto"],table)
		makeTable(prefix["local"],table)
		moveLastEntry(table)
func makeTable(tablePrefix,table):
	var metadata = tables[table].duplicate(true)
	metadata["del"] = { "data_type" : "int" } #used to destroy unwanted lines
	DB.create_table(tablePrefix+table,metadata)
func clearAutosaveTables():
	for table in tables:
		DB.query("DROP TABLE " + prefix["auto"]+table)
func makeAutoSaveTables():
	for table in tables:
		makeTable(prefix["auto"],table)
		moveLastEntry(table)
func moveLastEntry(table):
	var row = {}
	var data = []
	if tables[table].has("id"):
		var rowKeys = tables[table].keys()
		data = DB.select_rows(prefix["local"]+table,"id=(select max(id) from " + prefix["local"]+table + ")",rowKeys)
		if (data.size() > 0):
			data = data[0]
			for key in rowKeys:
				row[key] = data[key]
			DB.insert_row(prefix["auto"]+table,row)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
