extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var Status = load("res://Constants/Status.gd")
var configFilePath = ["res://config.cfg"]
var configFile : ConfigFile
var configData = {}
var changedSettings = {}
#First: Some Config file stuff to keep this from being hardcoded.
func loadConfig(filepath):
	var test = configFile.load(filepath)
	if test != OK:
		print("Config File Error! \n File:\t" + filepath + "\nError:\t"+ var_to_str(test))
	else:
		for section in configFile.get_sections():
			if !configData.has(section):
				if section != "Config":
					configData[section] = []
			if !changedSettings.has(section):
				changedSettings[section] = {}
			for key in configFile.get_section_keys(section):
				if section == "Config":
					configFilePath.push_back(configFile.get_value(section,key))
				else:
					configData[section].push_back(
						setting.new(
							key,configFile.get_value(section,key)
							)
						)
					changedSettings[section][key] = true

#func saveConfig():
#	for section in changedSettings:
#		for entry in changedSettings[section]:
#			configFile.set_value(section,entry,configData[section][entry])
#	var error = configFile.save(configFilePath)
#	return error
	
func setupDefaultConfig():
	addSetting("Settings", "saveFolder","user://Saves/")
	addSetting("Settings", "modFolder", "res://Mods")
	addSetting("Settings", "saveFileType", "PlainJson")

func addSetting(section,setting_name,value):
	if !configData.has(section):
		configData[section] = []
	configData[section].push_back(setting.new(setting_name,value))
	pass
	
func get_setting(section,setting_name):
	for setting_value in configData[section]:
		if setting_value._get_name() == setting_name:
			return setting_value
# Called when the node enters the scene tree for the first time.
func _ready():
	configFile = ConfigFile.new()
	setupDefaultConfig()
	for file in configFilePath:
		loadConfig(file)
		if file==configFilePath[0]:
			changedSettings = {} #The default settings should not be saved
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
