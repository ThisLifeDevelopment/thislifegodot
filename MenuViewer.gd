extends TabContainer
var tabs = ["Loading Screen","Mods","Credits","Worlds","Settings"]

func _ready():
	for child in get_children():
		child.connect("changePage", Callable(self, "_change_page"))

func _change_page(page: String):
	current_tab = tabs.find(page)
