extends Node
class_name Game_Manager
#Classes I need
var Menu = preload("res://Menu.tscn")
var Game = preload("res://Spatial.tscn")


var currentScene;
var loadingScene;


func _init():
	loadMenu()
# warning-ignore:return_value_discarded
	SignalBus.connect("LaunchGame", Callable(self, "loadGame"))

func loadMenu():
	loadingScene = Menu.instantiate()
	make_scene_current()

func loadGame(_context):
	loadingScene = Game.instantiate()
	make_scene_current()
	
func make_scene_current():
	add_child(loadingScene)
	unload_scene(currentScene)
	currentScene = loadingScene
	loadingScene = null
	
func unload_scene(scene_to_unload : Node):
	if scene_to_unload != null:
		scene_to_unload.queue_free()
