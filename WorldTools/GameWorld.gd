extends Node
class_name GameWorld
#Include relevant constants:
var Status = load("res://Constants/Status.gd")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

@export var ParcelSize = 9
@export var areaMap : CompressedTexture2D
var areaImage
var deadPixels : Vector2

var limits = {}
#Dictionary of loaded areas. Does actually need to be a dictionary
var areas = {}

func verifyareaMap():
	deadPixels = Vector2(
		(areaMap.get_width()-1)%(ParcelSize-1),
		(areaMap.get_height()-1)%(ParcelSize-1)
		)
	areaImage = areaMap.get_image()
	return true
	

func addArea(x,y,area : Parcel):
	add_child(area)
	if (!areas.has(x)):
		areas[x] = {}
	areas[x][y] = area
	area.launch(x,y,ParcelSize,getMapChunk(x,y,ParcelSize))
	
# Called when the node enters the scene tree for the first time.
func _ready():
	RenderingServer.set_debug_generate_wireframes(true)
	verifyareaMap()
	for x in (areaMap.get_width()-deadPixels[0]-1)/(ParcelSize-1):
		for y in (areaMap.get_height()-deadPixels[1]-1)/(ParcelSize-1):
			addArea(x,y,Parcel.new())

	pass # Replace with function body.

func getMapChunk(x,y,size):
	var mapChunk = []
	var xoffset=x*(size-1)
	var yoffset=y*(size-1)
	for xpos in range(size):
		mapChunk.push_back([])
		for ypos in range(size):
			mapChunk[xpos].push_back(areaImage.get_pixel(xoffset+xpos,yoffset+ypos).r8)
	#		if (x==0||x==size-1||y==0||y==size-1):
	#			mapChunk[x].push_back(255)
	#		else:
	#			mapChunk[x].push_back(0)
	return mapChunk

func update():
	for x in areas:
		for y in areas[x]:
			areas[x][y].update()
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
