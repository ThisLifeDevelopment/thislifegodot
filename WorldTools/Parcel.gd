extends Node3D



#The smallest unit of land that can be loaded.
class_name Parcel

#Status Enum:
var  Status = preload("res://Constants/Status.gd").Status
#Custom Shader:
var GroundShader = preload("res://WorldTools/GroundShader.tres")
#Public state
var status
#Positioning Information
var size
var offsetx
var offsety

var heightMap = [[]] #The map of the ground
var ground : MeshInstance3D
var material
var mesh : PlaneMesh
var texture
var image
# Called when the node enters the scene tree for the first time.
func _ready():
	instantiateGround()
	status = Status.UNLOADED#Parsel is not yet ready when it loads
	pass # Replace with function body.

func _notification(what):
	if what == NOTIFICATION_PREDELETE:
		removeGround()
		pass

func verifyHeightmap():
	if (heightMap.size()!=size):
		return Status.FAILURE
	if (heightMap[0].size()!=size):
		return Status.FAILURE
	return Status.SUCCESS

func launch(x,y,lotsize,map):
	status = Status.BUSY
	offsetx=x
	offsety=y
	size=lotsize
	heightMap=map
	
	if (verifyHeightmap() == Status.FAILURE):
		return Status.FAILURE
	instantiateGround()
	buildground()
	
	#Load heightmap and items on it
	status = Status.LOADED
	return Status.SUCCESS

func update():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func instantiateGround():
	ground = MeshInstance3D.new()
	self.add_child(ground)
	mesh = PlaneMesh.new()
	image = Image.new()
	material = ShaderMaterial.new()
	texture = ImageTexture.new()
func removeGround():
	pass

func buildground():
	mesh.size = Vector2(size,size)
	mesh.subdivide_depth = size -2
	mesh.subdivide_width = size -2
	image = Image.create_from_data(size,size,false,Image.FORMAT_R8,makeImageData())
	texture = ImageTexture.create_from_image(image)
	#texture.flags = METHOD_FLAG_STATIC #turn off compression and repitition
	material.shader = GroundShader
	material.set_shader_parameter("height_scale", 1.0)
	material.set_shader_parameter("size",size)
	material.set_shader_parameter("noise",texture)
	ground.mesh = mesh
	ground.material_overlay = material
	ground.translate(Vector3(offsetx*size,0.0,offsety*size))

func makeImageData():
	var temparray = []
	var imageData= [PackedByteArray()]
	for x in range(size):
		for y in range(size):
			temparray.push_back(heightMap[y][x])
	imageData = temparray
	return imageData
