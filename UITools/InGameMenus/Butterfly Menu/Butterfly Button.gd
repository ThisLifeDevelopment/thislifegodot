extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var center
var pieceCount
var radius

var angle_from = 270
var angle_to = 450

var color_primary = Color(0,0,0)
var color_secondary = Color(0,0,1)

@export var button_radius = 40 #in godot position units
@export var radial_width = .4 #in godot position units

# Called when the node enters the scene tree for the first time.
func _ready():
	center = Vector2(size.x/2,size.y)
	radius = size.y
	print(size)
	#CanvasItem.update()
	pass
	
func _innerArc():
	draw_circle_arc(radius/2, angle_from, angle_to, color_secondary)
	pass
	
func _outerArc():
	draw_circle_arc(radius, angle_from, angle_to, color_primary)

func place_buttons():
	var menus = get_children()
	var buttons = []
	#pull the buttons from the menus
	for menu in menus:
		buttons.append(menu.get_node("Button"))

		#Stop before we cause problems when no buttons are available
	if buttons.size() == 0:
		return

	#Amount to change the angle for each button
	var angle_offset = (2*PI)/buttons.size() #in degrees

	var angle = 0 #in radians
	for btn in buttons: 
		#calculates the buttons location on the circle
		var circle_pos = Vector2(button_radius, 0).rotated(angle)

		#set button's position
		#>we want to center the element on the circle. 
		#>to do this we need to offset the calculated x and y respectively by half the height and width
		btn.position = circle_pos-(btn.get_size()/2)
		#Advance to next angle position
		angle += angle_offset


#utility function for adding buttons and recalculating their positions
#TODO: Should probably just use a signal to run place_button on any tree change
func add_button(btn):
	add_child(btn)
	place_buttons()

func _draw():
	_outerArc()
	_innerArc()
	place_buttons()
	# Your draw commands here
	pass
	
func draw_circle_arc(circle_radius, from_angle, to_angle, color):
	var nb_points = 32
	var points_arc = PackedVector2Array()
	var colors = PackedColorArray([color])
	
	for i in range(nb_points + 1):
		var angle_point = deg_to_rad(from_angle + i * (to_angle-from_angle) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * circle_radius)

	for index_point in range(nb_points):
		draw_line(points_arc[index_point], points_arc[index_point + 1], color)
	draw_polygon(points_arc,colors)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
