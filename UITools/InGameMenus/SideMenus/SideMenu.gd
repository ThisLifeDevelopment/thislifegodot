extends BoxContainer


# Declare member variables here. Examples:
var ToggleButton
var contentPanel
var panelStorage
var this
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	contentPanel = get_node("PanelStore/PanelContainer") 
	ToggleButton = get_node("DisplayToggle")
	panelStorage = get_node("PanelStore")
	ToggleButton.connect("pressed", Callable(self, "_toggleContentPanel"))
	pass # Replace with function body.
func _toggleContentPanel():
	print_tree_pretty()
	print(contentPanel)
	if (panelStorage.get_child_count() > 0):
		panelStorage.remove_child(panelStorage.get_child(0))
	else:
		panelStorage.add_child(contentPanel)



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
