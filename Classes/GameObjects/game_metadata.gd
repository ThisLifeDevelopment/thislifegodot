class_name GameMetadata

var name : String
var previewImage : Texture2D
var description : String
var created : String
var modified : String
var id : int
const DefaultImage = "res://icon.png"

func _init():
	name="example"
	previewImage = load(DefaultImage)
	created = Time.get_time_string_from_system()
	modified = Time.get_time_string_from_system()
	description = "No description available"
	id = -1
