class_name setting

var setting_name : get = _get_name, set = _set_name
func _set_name(value):
	setting_name = value

func _get_name():
	return setting_name

var setting_value : get = _get_value, set = _set_value
var dirty_value
func _set_value(value):
	dirty_value = value

func _get_value():
	if dirty_value:
		return dirty_value
	else:
		return setting_value

func _init(name,value):
	setting_name = name
	setting_value = value
	pass

func save():
	emit_signal("saveme")
	setting_value = dirty_value

func reset():
	dirty_value = setting_value

signal saveme
