@tool
extends EditorPlugin


func _enter_tree():
	add_custom_type("ClickyPanel","PanelContainer",preload("controls/ClickyPanel.gd"),preload("icon.png"))
	pass


func _exit_tree():
	remove_custom_type("ClickyPanel")
	pass
