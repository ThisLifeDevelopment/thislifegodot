@tool
extends EditorPlugin

var types_added=[]

func _enter_tree():
	add_custom_type("Butterfly_Bar","HBoxContainer", preload("butterfly_bar.gd"), preload("ButterflyIcon.png"))
	types_added.append("Butterfly_Bar")
	add_custom_type("ButterflyMenuOption","Button",preload("ButterflyMenuOption.gd"),preload("ButterflyMenuOption.png"))
	types_added.append("ButterflyMenuOption")
	
	pass


func _exit_tree():
	for type in types_added:
		remove_custom_type(type)
	pass
