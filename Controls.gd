extends Node3D

class_name Controls

var DebugTools = load("res://InterfaceTools/DebugTools.gd")
var KeyMap = load("res://InterfaceTools/KeyMap.gd")
var GameWorld = load("res://WorldTools/GameWorld.gd")
var camera : CameraMover
var debugInstance
var mapped : KeyMap
var LetCameraMove = true

var world : GameWorld
# Called when the node enters the scene tree for the first time.
func _ready():
	debugInstance = DebugTools.new()
	mapped = KeyMap.new()
	self.add_child(mapped)
	self.add_child(debugInstance)
	world = self.get_node("World")
	camera = get_node("Camera3D")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#MISSING: Input blocking. 
	#check for modifiers
	if Input.is_key_pressed(KEY_CTRL):
		pass
	else: #It is safe to move the camera
		var x = 0
		var y = 0
		var z = 0
		var tilt = 0
		if Input.is_key_pressed(mapped.CAMERARIGHT):
			x+=1
		if Input.is_key_pressed(mapped.CAMERALEFT):
			x-=1
		if Input.is_key_pressed(mapped.CAMERABACK):
			z+=1
		if Input.is_key_pressed(mapped.CAMERAFORWARD):
			z-=1
		if Input.is_key_pressed(mapped.CAMERAUP):
			y+=1
		if Input.is_key_pressed(mapped.CAMERADOWN):
			y-=1
		camera.moveview(x*delta,y*delta,z*delta)
		if Input.is_key_pressed(mapped.CAMERACLOCKWISE):
			tilt+=1
		if Input.is_key_pressed(mapped.CAMERACOUNTERCLOCKWISE):
			tilt-=1
		camera.rotateview(tilt*delta)
	#camera movements:
	pass

func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed:
			if event.keycode == KEY_ESCAPE:
				debugInstance.toggle_debug()
				world.update()
				#reloadWorld
			
