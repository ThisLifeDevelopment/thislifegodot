extends Node

class_name KeyMap

var CAMERAUP = KEY_UP
var CAMERADOWN = KEY_DOWN
var CAMERAFORWARD = KEY_SPACE
var CAMERABACK = KEY_SHIFT
var CAMERALEFT = KEY_LEFT
var CAMERARIGHT = KEY_RIGHT
var CAMERACLOCKWISE = KEY_E
var CAMERACOUNTERCLOCKWISE = KEY_Q

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
