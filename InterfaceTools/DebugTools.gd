extends Node


# Declare member variables here. Examples:
var wireframe = false
# var b = "text"

func toggle_debug():
	toggle_wireframe()
	print("Debug toggled")
	pass
	
	
func toggle_wireframe():
	var vp = get_viewport()
	vp.debug_draw = (vp.debug_draw + 1 ) % 4
	pass
# Called when the node enters the scene tree for the first time.
func _ready():
	RenderingServer.set_debug_generate_wireframes(true)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
