extends Camera3D
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
class_name CameraMover

var movementSpeed = 1.0 #units per second
var rotationSpeed = 1.0
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func moveview(x,y,z):
	self.translate(Vector3(x,y,z)*movementSpeed)
	pass
	
func rotateview(direction):
	self.rotate_z(direction)
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
