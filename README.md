#This Life Godot Test
====================

ThisLife is a SimsLike game intended for the general public. 

Build a home, raise a family. 

If we're lucky, I'll even get a strong economy and city building mechanics built!

MOST objects in this game will need to be added as mods. Some "bland" items will be included as fallback items for if mods are removed.

Each world should have its own mod set, allowing various types of play to be available from session to session without massive configuration changes! :D

#Contact:

Service Desk Email: contact-project+thislifedevelopment-thislifegodot-39961914-issue-@incoming.gitlab.com

Bug reports: Please open an issue on https://gitlab.com/ThisLifeDevelopment/thislifegodot

#Special Thanks:

Special thanks to some of the people who won't show up in the Git history, but absolutely played a critical role in helping build and design this game! 

* Minako -- Very helpful with designing the UI. (And keeping my sanity in general.)

* Dan Lunsford -- Designs pretty houses. I'm modeling the design of the construction tools off his process!

zsky2000 on itch.io -- Built lots of models that I was able to use during development and testing! Thanks zsky2000! Find them here: https://zsky2000.itch.io/low-poly-outdoor-decorations


