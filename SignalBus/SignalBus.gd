extends Node
#Used for general testing

#warning-ignore:UNUSED_SIGNAL
signal testSignal (context)

#Context Switchinng

#warning-ignore:UNUSED_SIGNAL
signal LaunchGame (context)
#warning-ignore:UNUSED_SIGNAL
signal GameSetup (context)
#warning-ignore:UNUSED_SIGNAL
signal MainMenu (context)


func raiseSignal(name : String, context : Dictionary):
	emit_signal(name,context)
