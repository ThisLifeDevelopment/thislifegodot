class_name fake_SQLite
### SQLite can't be doubled via the doubler
### This class provides an alternative

const QUIET = 0

const NORMAL = 1

const VERBOSE = 2

const VERY_VERBOSE = 3
var path : String: get = get_db_path, set = set_db_path

func set_db_path(value : String) -> void:
	path = value

func get_db_path() -> String:
	return path

var error_message : String: get = get_error_message, set = set_error_message

func set_error_message(message: String) -> void:
	error_message = message
	
func get_error_message() -> String:
	return error_message

var default_extension :String: get = get_default_extension, set = set_default_extension

func set_default_extension(extension : String) -> void:
	default_extension = extension

func get_default_extension() -> String:
	return default_extension

var foreign_keys : bool: get = get_foreign_keys, set = set_foreign_keys

func set_foreign_keys(value : bool) -> void:
	foreign_keys = value

func get_foreign_keys() -> bool:
	return foreign_keys

var read_only : bool: get = get_read_only, set = set_read_only

func set_read_only(value : bool) -> void:
	read_only = value
	
func get_read_only() -> bool:
	return read_only


## Contains the results from the latest query **by value**; 
## This property is safe to use when looping successive queries as it does not get overwritten by any future queries.
var query_result : Array: get = get_query_result, set = set_query_result

func set_query_result(value : Array) -> void:
	query_result = value

func get_query_result() -> Array:
	return query_result


## Contains the results from the latest query **by reference** and is, as a direct result, cleared and repopulated after every new query.
var query_result_by_reference : Array: get = get_query_result_by_reference, set = set_query_result_by_reference

func set_query_result_by_reference(value : Array) -> void:
	query_result_by_reference = value

func get_query_result_by_reference() -> Array:
	return query_result_by_reference


## Exposes both the `sqlite3_last_insert_rowid()`- and `sqlite3_set_last_insert_rowid()`-methods to Godot as described [here](https://www.sqlite.org/c3ref/last_insert_rowid.html) and [here](https://www.sqlite.org/c3ref/set_last_insert_rowid.html) respectively.
var last_insert_rowid : int: get = get_last_insert_rowid, set = set_last_insert_rowid

func set_last_insert_rowid(value : int) -> void:
	last_insert_rowid = value
	
func get_last_insert_rowid() -> int:
	return last_insert_rowid


##    The verbosity_level determines the amount of logging to the Godot console that is handy for debugging your (possibly faulty) SQLite queries.
##
##   | Level            | Description                                 |
##   |----------------- | ------------------------------------------- |
##   | QUIET (0)        | Don't print anything to the console         |
##   | NORMAL (1)       | Print essential information to the console  |
##   | VERBOSE (2)      | Print additional information to the console |
##   | VERY_VERBOSE (3) | Same as VERBOSE                             |
##
##   ***NOTE:** VERBOSE and higher levels might considerably slow down your queries due to excessive logging.*
var verbosity_level : int: get = get_verbosity_level, set = set_verbosity_level

func set_verbosity_level(value) -> void:
	verbosity_level = value

func get_verbosity_level() -> int:
	return verbosity_level


## Opens a connection to the database. Be sure to set the path first.
var open_db_calls = []

func open_db() -> bool:
	open_db_calls.append({
		"path": path,
		"extention" : default_extension,
		})
	return true
	
var close_db_calls = []
## Closes connection to the database.
func close_db() -> bool:
	close_db_calls.append({
		"path":path,
		"extension": default_extension
	})
	return true
	
var query_calls = []
## Passes a query as-is to the database
func query(query_string : String) -> bool:
	query_calls.append({"query":query_string})
	return true

var query_with_binding_calls = []
func query_with_bindings(query_string : String, param_bindings : Array) -> bool:
	query_with_binding_calls.append(
		{
			"query": query_string,
			"params": param_bindings,
		}
	)
	return true

var create_table_calls = []
func create_table(table_name: String, table_dictionary: Dictionary) -> bool:
	create_table_calls.append({
		"table_name": table_name, 
		"table_dictionary": table_dictionary
		})
	return true

## Drops an existing table
var drop_table_calls = []
func drop_table(table_name: String) -> bool:
	drop_table_calls.append(table_name)
	return true

var insert_row_calls = []
func insert_row(table_name: String, row_dictionary : Dictionary) -> bool:
	insert_row_calls.append({
		"table_name": table_name,
		 "row_dictionary":row_dictionary
		})
	return true

var insert_rows_calls = []
func insert_rows(table_name: String, row_array : Array) -> bool:
	insert_row_calls.append({
		"table_name":table_name,
		"row_array":row_array
		})
	return true

var select_rows_returns = []
func select_rows(table_name : String, query_conditions: String, selected_columns: Array) -> Array:
	select_rows_returns.append({
		"table_name":table_name,
		"query_conditions":query_conditions,
		"selected_columns":selected_columns
		
	})
	return select_rows_returns


##    With the `updated_row_dictionary`-variable adhering to the same table schema & conditions as the `row_dictionary`-variable defined previously.
var update_rows_calls = []
func update_rows(table_name : String, query_conditions: String, updated_row_dictionary: Dictionary) -> bool:
	update_rows_calls.append({
		"table_name": table_name,
		"query_conditions":query_conditions,
		"updated_row_dictionary":updated_row_dictionary
	})
	return true


## Boolean success = **delete_rows(** String table_name, String query_conditions **)**
var delete_rows_calls = []
func delete_rows(table_name: String, query_conditions: String) -> bool:
	delete_rows_calls.append({
		"table_name":table_name,
		"query_conditions":query_conditions
	})
	return true

var import_from_json_calls = []
func import_from_json(import_path: String) -> bool:
	import_from_json_calls.append({
		"import_path":import_path
	})
	return true

var export_to_json_calls = []
func export_to_json(export_path : String) -> bool:
	export_to_json_calls.append({
		"export_path":export_path
		})
	return true


var create_function_calls = []
func create_function(function_name: String, function_reference: FuncRef, number_of_arguments: int) -> bool:
	create_function_calls.append({
		"function_name":function_name,
		"function_reference":function_reference,
		"number_of_arguments":number_of_arguments
	})
	return true

var get_autocommit_calls = []
func get_autocommit() -> int:
	get_autocommit_calls.append({})
	return 1

# Constructor
func _init():
	pass
