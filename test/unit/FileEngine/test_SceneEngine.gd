extends GutTest

var saveEngine : SaveEngine
# Declare test objects here
#var TestClass=load("res://example/class")
#var testObject = obj

#Doubles (Similar to Mocks)
var SQLite = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")
var doubledDB
var DBFaker = preload("res://test/tools/fakes/fakeSQLite.gd")
#var doubledNode = double(Node).new()
#var doubledMyClass = double(MyClass)
#set up

func before_each():
	doubledDB = DBFaker.new()
	saveEngine = SaveEngine.new()
	saveEngine.DB = doubledDB
	gut.p("ran setup", 2)

func after_each():
	saveEngine.free()
	gut.p("ran teardown", 2)

func before_all():
	gut.p("ran run setup", 2)

func after_all():
	print_orphan_nodes()
	gut.p("ran run teardown", 2)

#tests
#This will be an array later

# initialize

func test_get_saves_returns_a_dictionary_even_when_there_are_no_saves():
	doubledDB.query_result = []
	assert_eq_deep(saveEngine.get_saves(),{})
	
	
func test_get_saves_returns_a_dictionary_of_game_metadatas_indexed_by_id():
	doubledDB.query_result = [{"saveName":"name","ID":1,"Created":"0","Updated":"0","previewImage": ImageTexture.new(),"Description":"Test Data"}]
	#saveName, Created, Updated, ID, previewImage, description
	var return_val = saveEngine.get_saves()
	
	assert_eq(typeof(return_val[1]),typeof(GameMetadata.new()))
	
	
func test_get_save_returns_a_stored_games_metadata():
	pending()
	
func test_get_save_returns_null_if_no_game_exists():
	pending()
	
func test_store_save_metadata_stores_a_games_metadata():
	pending()
	

# 
#Mocks


#Helper Functions

