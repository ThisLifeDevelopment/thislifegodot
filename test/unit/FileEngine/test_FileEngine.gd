extends GutTest

var fileEngine : FileEngine
# Declare test objects here
#var TestClass=load("res://example/class")
#var testObject = obj

#Doubles (Similar to Mocks)
var doubledSaveEngine 
var doubledConfigSystem
#var doubledNode = double(Node).new()
#var doubledMyClass = double(MyClass)
#set up

func before_each():
	fileEngine = FileEngine
	fileEngine.saveMachine.free()
	fileEngine.configSystem.free()
	doubledSaveEngine = double(SaveEngine).new()
	doubledConfigSystem = double("res://FileEngine/ConfigSystem.gd").new()
	fileEngine.saveMachine = doubledSaveEngine
	fileEngine.configSystem = doubledConfigSystem 
	gut.p("ran setup", 2)

func after_each():
	gut.p("ran teardown", 2)

func before_all():
	
	gut.p("ran run setup", 2)

func after_all():
	fileEngine._ready() #reset the file engine after all we've done to it
	gut.p("ran run teardown", 2)

#tests

func test_get_saves_returns_a_value_from_the_save_engine():
	stub(doubledSaveEngine,'get_saves').to_return("saves")
	var value = fileEngine.get_saves()
	assert_eq(value,"saves")
	assert_called(doubledSaveEngine,"get_saves")
	
	#func get_saves()


	#func saveGame():
	#func saveObject(type,object):
	#func getObject(type,id):
	#func startGame(name):
	#func createSave(name:String):
#Mocks


#Helper Functions

