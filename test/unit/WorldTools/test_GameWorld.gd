extends GutTest

class_name testGameWorld
# Declare test objects here
var GameWorld=load("res://WorldTools/GameWorld.gd")
var testObject
#Doubles (Similar to Mocks)
var doubledStreamTexture
var doubledImage
#var doubledMyClass = double(MyClass)
#set up
func before_each():
	doubledStreamTexture = double(CompressedTexture2D)
	stub(doubledStreamTexture,'get_data').to_do_nothing()
	testObject = GameWorld.new()
	testObject.areaMap = doubledStreamTexture
	testObject.areaImage = Image.new()
	var imageData = testImageData()
	testObject.areaImage.create_from_data(12,12,false,Image.FORMAT_R8,imageData)
	
	gut.p("ran setup", 2)
	

func after_each():
	testObject.free()
	gut.p("ran teardown", 2)

#tests

func test_mapChunks_next_to_eachother_on_the_x_share_first_depth_nodes():
	var obj1 = testObject.getMapChunk(0,0,4)
	var obj2 = testObject.getMapChunk(1,0,4)
	assert_eq(obj1[3],obj2[0])
	
func test_mapChunks_next_to_eachother_on_the_y_share_second_depth_nodes():
	var obj1 = testObject.getMapChunk(1,1,4)
	var obj2 = testObject.getMapChunk(1,2,4)
	assert_eq(obj1[2][3],obj2[2][0])
	
	
#Mocks

#Helper Functions
func testImageData():
	var tempdata = []
	var imageData= [PackedByteArray()]
	for i in range(12):
		for j in range(12):
			tempdata.push_back(12*i+j)
	imageData = tempdata
	return imageData
