extends GutTest

#class_name test_Parcel

var Parcel = load("res://WorldTools/Parcel.gd")
var Status = load("res://Constants/Status.gd").Status
var ParcelToTest : Parcel

func before_each():
	ParcelToTest = Parcel.new()
	add_child(ParcelToTest)
	gut.p("ran setup", 2)

func after_each():
	ParcelToTest.free()
	gut.p("ran teardown", 2)


func test_parcel_can_load_with_valid_settings():
	var map = makeMap(8,8)
	var result = ParcelToTest.launch(0,0,8,map)
	assert_eq(result,Status.SUCCESS,"Valid launch prameteres")

func test_parcel_cannot_be_made_with_short_array_depth_1():
	var map = makeMap(4,8)
	var result = ParcelToTest.launch(0,0,8,map)
	assert_eq(result,Status.FAILURE,"Short array, first depth")
	
func test_parcel_cannot_be_made_with_short_array_depth_2():
	var map = makeMap(8,4)
	var result = ParcelToTest.launch(0,0,8,map)
	assert_eq(result,Status.FAILURE,"Short array, second depth")
	
func test_parcel_cannot_be_made_with_large_array_depth_1():
	var map = makeMap(9,8)
	var result = ParcelToTest.launch(0,0,8,map)
	assert_eq(result,Status.FAILURE,"long array, first depth")
	
func test_parcel_cannot_be_made_with_large_array_depth_2():
	var map = makeMap(9,8)
	var result = ParcelToTest.launch(0,0,8,map)
	assert_eq(result,Status.FAILURE,"Short array, second depth")

func makeMap(depth1:int,depth2:int):
	var map = []
	for a in range(depth1):
		map.append([])
		for _b in range(depth2):
			map[a].append(1)
	return map
