extends GutTest

var Root = preload("res://thislife-game-manager.gd")
var test_root : Game_Manager
var Menu = preload("res://Menu.tscn")
var Game = preload("res://Spatial.tscn")

var fake_save_machine
# Declare test objects here
#var TestClass=load("res://example/class")
#var testObject = obj

#Doubles (Similar to Mocks)
#var doubledNode = double(Node).new()
#var doubledMyClass = double(MyClass)
#set up


func before_each():
	fake_save_machine = double(SaveEngine).new()
	FileEngine.setSaveMachine(fake_save_machine)
	stub(fake_save_machine,"get_saves").to_return({})
	test_root = Root.new()
	add_child(test_root)
	gut.p("ran setup", 2)

func after_each():
	test_root.free()
	gut.p("ran teardown", 2)

func after_all():
	print_orphan_nodes()
#tests


func test_root_begins_loading_menu_first():
	assert_eq(test_root.currentScene.get_scene_file_path(), Menu.get_path() )

func test_root_loads_game_when_launch_game_event_is_raised():
	var bus = SignalBus
	bus.raiseSignal("LaunchGame",{"source":self})
	assert_eq(test_root.currentScene.get_scene_file_path(),Game.get_path()
	)
	
#Mocks


#Helper Functions

