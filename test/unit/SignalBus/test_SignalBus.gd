extends GutTest

var SignalBus = load("res://SignalBus/SignalBus.gd")
# Declare test objects here
var signal_bus

func before_each():
	signal_bus = SignalBus.new()
	add_child(signal_bus)
	gut.p("ran setup", 2)

func after_each():
	signal_bus.free()
	gut.p("ran teardown", 2)

#tests

func test_SignalBus_raises_signals_when_rasieSignal_is_called():
	watch_signals(signal_bus)
	signal_bus.raiseSignal("testSignal",{"testobject":self})
	assert_signal_emitted_with_parameters(signal_bus,"testSignal",[{"testobject":self}])
	
#Mocks


#Helper Functions

