extends GutTest


# Declare test objects here
#var TestClass=load("res://example/class")
#var testObject = obj
var TestClass = load("res://Menus/Worlds/Worlds.gd")
var TestSceneClass = load("res://Menus/Worlds/Worlds.tscn")
var testObject : WorldsMenu
#Doubles (Similar to Mocks)
var fake_save_engine
var real_save_engine : SaveEngine
#var doubledNode = double(Node).new()
#var doubledMyClass = double(MyClass)
#set up
var game : GameMetadata

func before_each():
	
	#Sabotage the singleton
	fake_save_engine = double("res://FileEngine/SaveEngine.gd").new()
	FileEngine.setSaveMachine(fake_save_engine)
	stub(fake_save_engine,"get_saves").to_return({})
	assert(FileEngine.saveMachine != null)
	
	testObject = TestSceneClass.instantiate()
	add_child(testObject)
	gut.p("ran setup", 2)

func after_each():
	testObject.free()
	gut.p("ran teardown", 2)

func before_all():
	game = GameMetadata.new()
	game.id=7
	gut.p("ran run setup", 2)

func after_all():
	FileEngine.setSaveMachine(SaveEngine.new())
	gut.p("ran run teardown", 2)

#tests

func test__load_worlds_includes_a_newgame_world():
	stub(fake_save_engine,"get_saves").to_return({7:game})
	testObject._load_worlds()
	for world in testObject.worlds:
		if testObject.worlds[world].id == -1:
			pass_test("List of worlds contains special id of -1, indicatig a new game option")
	
func test__load_worlds_includdes_any_available_worlds():
	
	stub(fake_save_engine,"get_saves").to_return({
		7:game
		})
	testObject._load_worlds()
	for world in testObject.worlds:
		if testObject.worlds[world].id == 7:
			pass_test("List of worlds contains a world that is not a newgame world")
#Mocks


#Helper Functions

