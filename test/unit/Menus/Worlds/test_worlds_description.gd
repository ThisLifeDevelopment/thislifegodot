extends GutTest

var testobj : WorldDescription
var testWorlds
# Declare test objects here
#var TestClass=load("res://example/class")
#var testObject = obj

#Doubles (Similar to Mocks)
#var doubledNode = double(Node).new()
#var doubledMyClass = double(MyClass)
#set up

func before_each():
	testobj = WorldDescription.new()
	add_child(testobj)
	watch_signals(SignalBus)
	gut.p("ran setup", 2)

func after_each():
	testobj.free()
	clear_signal_watcher()
	gut.p("ran teardown", 2)

func before_all():
	testWorlds = [];
	testWorlds.append(GameMetadata.new())
	var test_world = GameMetadata.new()
	test_world.id = 1
	test_world.description = "Test World"
	testWorlds.append(test_world)
	gut.p("ran run setup", 2)

func after_all():
	print_orphan_nodes()
	gut.p("ran run teardown", 2)

func test_selecting_a_world_displays_world_information_in_appropriate_fields():
	testobj.update_contents(testWorlds[1])
	assert_eq(testWorlds[1].description,testobj.descriptionBox.text)
	assert_eq(testWorlds[1].previewImage,testobj.image_box.texture)

#tests
func test_pressing_play_with_existing_game_selected_launches_new_game():
	testobj.update_contents(testWorlds[1])
	testobj.playButton.emit_signal("pressed")
	assert_signal_emitted(SignalBus,"LaunchGame")
	

func test_pressing_play_with_new_game_selected_launches_setup_menu():
	testobj.update_contents(testWorlds[0])
	pending()
#	var playButton = Button.new()
#	playButton.text = "Play"
#	side_panel.add_child(playButton)
func test_mods_button_opens_game_specific_mods_window():
	pending()
	
func test_mods_button_opens_game_specific_mods_window_for_new_game_if_nothing_selected():
	pending()
#	var modsButton = Button.new()
#	modsButton.text = "Mods"
#	side_panel.add_child(modsButton)
func test_settings_button_opens_game_specific_settings_window():
	pending()
	
func test_settings_button_opens_game_specific_settings_window_for_new_game_if_nothing_selected():
	pending()
#	var settingsButton = Button.new()
#	settingsButton.text = "Settings"
#	side_panel.add_child(settingsButton)
	
#Mocks


#Helper Functions

