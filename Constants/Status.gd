enum Status {
	UNLOADED, #Resource is empty and awaiting use
	BUSY,#Something is actively working with the resource.
	LOADED, #Resource is ready
	UNLOADING,#Used when a resource is ready to be unloaded
	SUCCESS,
	FAILURE,
}
